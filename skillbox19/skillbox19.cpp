﻿#include <iostream>

using namespace std;

class Animals
{
public:
    Animals() {}
    virtual void Voice() 
    {}
};
class Dog : public Animals
{
public:
    Dog() { dog = " WOOF, WOOF!"; }
    void Voice() override
    {
        cout << dog << endl;
    }
private:
    string dog;
};
class Cattel : public  Animals
{
public:
    Cattel() { cattel = " MOO! "; }
    void Voice() override
    {
        cout << cattel << endl;
    }
private:
    string cattel;
};
class Cat : public Animals
{
public:
    Cat() { cat = " MEOW! "; }
    void Voice() override
    {
        cout << cat << endl;;
    }
private:
    string cat;
};
int main()
{
    const int size = 3;
    Animals* arr[size] = { new Dog,new Cattel,new Cat};
    for (int i = 0; i < size; i++)
    {
        arr[i]->Voice();
    }
    system("pause");
}

